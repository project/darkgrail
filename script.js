if (Drupal.jsEnabled){
   $( function() {
     
   //borrowed from switchgrid.js on bluetrip theme by couzinhub
   $(document).ready(function(){
   $('a#switchgrid').click(function(){
    $("#page").toggleClass('showgrid');
    $(this).toggleClass('off')
    });
   });

   /*Popup xhtml valid external link, so you don't have to use target="_blank". Change your site name. Or else use, extlink.module.
   $("#fixed a[href^='http://']").not("a[href^='http://mysite.com']").not("a[href^='http://www.mysite.com']").click(function(){
    window.open(this.href,'external');
   	return false;
    });
   */
	

  //Create a compact searh form. Or else use compact_forms.module
    var search_name = 'type to search and hit...';
  $('#search #edit-search-theme-form-1').attr('value', search_name);
  $('#search #edit-search-theme-form-1').focus(function() {
    if ($(this).attr('value') == search_name) {
      $(this).attr('value', '');
    }
  });
  $('#search #edit-search-theme-form-1').blur(function() {
    if ($(this).attr('value') == '') {
      $(this).attr('value', search_name);
    }
  });


  });//function
}