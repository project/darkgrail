<?php

// Include the definition of zen_settings() and zen_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zen') . '/theme-settings.php';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function darkgrail_settings($saved_settings) {

  // Get the default values from the .info file.
  $defaults = zen_theme_get_default_settings('darkgrail');

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  /*
   * Create the form using Forms API: http://api.drupal.org/api/6
   */
  $form = array();
  $form['darkgrail_ads'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Activate ads unit supports '),
    '#default_value' => $settings['darkgrail_ads'],
    '#description'   => t('Load Universal Ad Package and Ad Unit Guidelines supports (ads.css). Useful during devs'),
  );
  
  $form['darkgrail_overrides'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Activate custom width overrides'),
    '#default_value' => $settings['darkgrail_overrides'],
    '#description'   => t('You must rename grail-overrides-rename.css to grail-overrides.css first, see <a href="!link">README</a>.', array('!link' => base_path() . path_to_theme() .'/README.TXT')),
  );
  
  $form['darkgrail_layout'] = array(
    '#type'          => 'radios',
    '#title'         => t('Sidebar dev width alternatives'),
    '#options'       => array(
                          'darkgrail_default' => t('Default'),
                          'darkgrail_140x300' => t('Fixed 140x300'),
                          'darkgrail_160x220' => t('Fixed 160x220'),
                          'darkgrail_170x170' => t('Fixed 170x170'),
                        ),
    '#default_value' => $settings['darkgrail_layout'],
    '#description'   => t('This will load grail-devwidth.css. Check "Default" to unload it on production and "Activate custom width overrides" above instead. Default is 130x230 on layout.css overriden by your grail-overrides.css. <br />Share your own and <a href="!link">let me know</a> to include yours in the next release.', array('!link' => 'http://gausarts.com/project/darkgrail')),
  );

 // Add the base theme's settings.
  $form += zen_settings($saved_settings, $defaults);

  // Remove some of the base theme's settings.
  unset($form['themedev']['zen_layout']); // We don't need to select the base stylesheet.

  // Return the form
  return $form;
}
